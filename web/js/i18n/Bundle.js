var languages = {
    PageTitle: "InfastView",
    VersionNo: "0.1",
    UserName: "usuario",
    Password: "contraseña",
    Login: "iniciar sesión",
    Footer: "INFASTview requiere la última versión de Google Chrome / Safari.",
    PatientName: "Nombre del paciente",
    PatientId: "ID del paciente",
    BirthDate: "Fecha de nacimiento",
    AccessionNumber: "Número de acceso",
    FromStudyDate: "fecha del estudio (Desde)",
    ToStudyDate: "fecha del estudio (Hasta)",
    StudyDate: "fecha de estudio",
    StudyDesc: "descripción del estudio",
    ReferPhysician: "médico que refiere",
    Modality: "modalidad",
    InstanceCount: "Instancias",
    Reset: "Resetear",
    Search: "Buscar",
    Layout: "diseño",
    Windowing: "Ventanas",
    Zoom: "Zoom",
    Move: "movimiento",
    ScoutLine: "Líneas Scout",
    ScrollImage: "Desplazar Imagen",
    Synchronize: "sincronizar",
    VFlip: "Rotar Vertical",
    HFlip: "Rotar Horizontal",
    LRotate: "Girar izquierda",
    RRotate: "Gira derecha",
    Invert: "Invertir",
    TextOverlay: "Sobreponer Texto ",
    FullScreen: "pantalla completa",
    MetaData: "Ver Meta-Data",
    Lines: "líneas",
    Server: "servidor",
    QueryParam: "Parámetros de Consulta",
    Preferences: "preferencias",
    Verify: "verificar",
    Add: "Agregar",
    Edit: "Editar",
    Delete: "Borrar",
    Description: "descripción",
    HostName: "Host Name",
    AETitle: "AE Title",
    Port: "Puerto",
    Retrieve: "Recuperar",
    Update: "actualizar",
    FilterName: "nombre del filtro",
    StudyDateFilter: "Filtrar por Fecha de Estudior",
    StudytimeFilter: "Filtrar por Hora de Estudio",
    ModalityFilter: "Filtrar por Modalidad",
    AutoRefresh: "Auto Refrescar",
    IOviyamCxt: "iOviyam Context",
    disclaimer: "Esta versión de Oviyam, al ser un software libre de código abierto (FOSS), no está certificada como un dispositivo médico comercial (FDA o CE-1).",
    limitation: "Verifique con la oficina de cumplimiento local las posibles limitaciones en su uso clínico.",
    Brain: "Cerebro",
    Abdomen: "abdomen",
    Lung: "pulmón",
    Bone: "Hueso",
    Head: "Cabeza/Cuello",
    Default: "defecto",
    Line: "Línea",
    Rectangle: "Rectángulo",
    Oval: "Oval",
    Angle: "Ángulo",
    DeleteAll: "Borrar las medidas",
    Workstation: "Estación de trabajo web DICOM",
    Version: "Versión",
    RememberMe: "Recuérdame",
    Creteria: "Sin criterios de búsqueda",
    Message: "No se seleccionó ningún filtro. La búsqueda podría tomar mucho tiempo. ¿Desea continuar?",
    ViewerPreference: "Preferencias del visor",
    prefetch: "Pre-cargar otros estudios del paciente seleccionado",
    sessTimeout: "Tiempo de Sesión"
};
